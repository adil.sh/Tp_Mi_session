﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="projet_Adil.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Page de Selection -->

        <div class="container-fluid">
         <div class="row">
&nbsp;  <div class="jumbotron text-center">
    <h1>Les Films</h1> 
    <p> Chercher les films dans la base de donnee.</p> 
  </div>

</div>

            <!-- Grid DB start -->
        <div class ="gridv">
              <div class="container text-center">
        <div class="row">
    
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies %>" SelectCommand="SELECT [Titre], [Id] FROM [films]"></asp:SqlDataSource>
            </div>
            <div class="col-md-6">
               
                 <br />
                <asp:DropDownList ID="DropDownList1" CssClass="drop1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Titre" DataValueField="Id" BackColor="#FFFFCC" Font-Bold="False" Font-Size="Medium" >
                </asp:DropDownList>
                 <br /><br />
                <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource2" Width="799px" Height="437px" BorderColor="#FF3300" BorderWidth="4px" CellPadding="5" GridLines="Vertical" BorderStyle="Dotted" ForeColor="Black" CssClass="grid" CellSpacing="2">
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#000099" ForeColor="Black" Font-Bold="True" />
                    <Fields>
                        <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                        
                        <asp:BoundField DataField="Titre" HeaderText="Titre" SortExpression="Titre" />
                        <asp:BoundField DataField="Categorie" HeaderText="Categorie" SortExpression="Categorie" />
                        <asp:BoundField DataField="Date_Publication" HeaderText="Date_Publication" SortExpression="Date_Publication" />
                        <asp:BoundField DataField="Image" HeaderText="Image" SortExpression="Image" />
                        <asp:BoundField DataField="Site" HeaderText="Site" SortExpression="Site" />
                    </Fields>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                </asp:DetailsView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies %>" SelectCommand="SELECT [Directeur], [Titre], [Categorie], [Date_Publication], [Image], [Site] FROM [films] WHERE ([Id] = @Id)">
                    <SelectParameters>
                        <asp:ControlParameter  ControlID="DropDownList1" Name="Id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>
            
            <!-- end -->
            
</div>
    
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="projet_Adil.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/JavaScript.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<!-- Page pour inserer des films -->
    <!-- Jumbotron pour inserer des films -->
        <div class="container-fluid">
         <div class="row">
&nbsp;  <div class="jumbotron text-center">
    <h1>Inserer une film</h1> 
    <p>Vous pouvez inerer des films de votre choix.</p> 
  </div>

</div>
            <!-- Insert DB code start -->
            <div class="gridvi">
                <br />
                <div class="row text-center">
       
       
       
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies %>" SelectCommand="SELECT [Id], [Nom] FROM [CateFilm]"></asp:SqlDataSource>

        <div class="col-sm-6 text-center">

            <asp:DetailsView ID="DetailsView1" runat="server" Width="799px" Height="437px" AutoGenerateRows="False" CellPadding="5" DataKeyNames="Id" DataSourceID="SqlDataSource1" OnItemDeleted="DetailsView1_ItemDeleted" OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnModeChanged="DetailsView1_ModeChanged" OnItemInserting="check_data" BackColor="#CCCCCC" BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="3px" CellSpacing="2" CssClass="grid" >
                <EditRowStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="Black" BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="2px" />
              
                <Fields>
                  
                    <asp:TemplateField HeaderText="Titre" SortExpression="Titre">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Titre") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txt_t" runat="server" Text='<%# Bind("Titre") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Titre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Categorie") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:DropDownList ID="DropDownList1" CssClass="drop" runat="server" DataSourceID="SqlDataSource3" DataTextField="Nom" DataValueField="Id" SelectedValue='<%# Bind("Categorie", "{0:N}") %>'>
                            </asp:DropDownList>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Categorie") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                    <asp:TemplateField HeaderText="DateDePub" SortExpression="DateDePub">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Date_Publication") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txt_date" runat="server" Text='<%# Bind("Date_Publication", "{0:yyyy-MM-dd}") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Date_Publication", "{0:yyyy-MM-dd}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Site" HeaderText="Site" SortExpression="Site" />
                    <asp:TemplateField HeaderText="Image" SortExpression="Image">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Image") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:FileUpload ID="fileup_image" style="margin-left:180px;" runat="server" onchange="get_file_name()" ClientIDMode="Static" />
                            <asp:TextBox ID="txt_img" style="display:none" runat="server" ClientIDMode="Static" Text='<%# Bind("Image") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Image") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <InsertItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
              
                <FooterStyle BackColor="green" ForeColor="green" />
                <HeaderStyle BackColor="green" Font-Bold="True" ForeColor="Black" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="Silver" ForeColor="Black" BorderColor="#FF9900" BorderStyle="Solid" BorderWidth="3px" Font-Bold="True" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies%>" DeleteCommand="DELETE FROM [films] WHERE [Id] = @Id" InsertCommand="INSERT INTO [films] ([Categorie], [Titre], [Directeur], [Date_Publication], [Site], [Image]) VALUES (@Categorie, @Titre, @Directeur, @Date_Publication, @Site, @Image)" SelectCommand="SELECT * FROM [films]" UpdateCommand="UPDATE [films] SET [Categorie] = @Categorie, [Titre] = @Titre, [Directeur] = @Directeur, [Date_Publication] = @Date_Publication, [Site] = @Site, [Image] = @Image WHERE [Id] = @Id">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Categorie" Type="Int32" />
                    <asp:Parameter Name="Titre" Type="String" />
                    <asp:Parameter Name="Directeur" Type="String" />
                    <asp:Parameter DbType="Date" Name="Date_Publication" />
                    <asp:Parameter Name="Site" Type="String" />
                    <asp:Parameter Name="Image" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                  <asp:Parameter Name="Categorie" Type="Int32" />
                    <asp:Parameter Name="Titre" Type="String" />
                    <asp:Parameter Name="Directeur" Type="String" />
                    <asp:Parameter DbType="Date" Name="Date_Publication" />
                    <asp:Parameter Name="Site" Type="String" />
                    <asp:Parameter Name="Image" Type="String" />
                    <asp:Parameter Name="Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:Label ID="Label5" runat="server" Font-Size="X-Large" ForeColor="#ff0000"></asp:Label>
            <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="#66FF33"></asp:Label>
            <asp:Image ID="Image1" runat="server" ImageUrl="images/ok.png" Height="114px" Visible="False" Width="129px" />
          <asp:Image ID="Image2" runat="server" ImageUrl="images/cancel.png" Height="114px" Visible="False" Width="129px" />
              <br />
        </div>
    </div>
            
</div>
   
    </div>
</asp:Content>

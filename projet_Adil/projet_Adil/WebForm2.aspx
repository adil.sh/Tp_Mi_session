﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="projet_Adil.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid">
         <div class="row">
&nbsp;  <div class="jumbotron text-center">
    <h1>Les Films</h1> 
    <p>Voila les films qui sont a l'affiche en ce moment</p> 
  </div>

</div>
        
        <div class="gridv">
          <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container text-center">
                <br /><br />
                <br />
                <div class="row" >
                    <div class="col-sm-6">
                        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1"  BorderColor="#CC9900" BorderWidth="10px" CellPadding="5" OnRowUpdated="GridView1_RowUpdated" OnRowDataBound="GridView1_RowDataBound" AllowPaging="True" PageSize="4" BorderStyle="none" Width="1105px" Height="300px" CssClass="grid">
                            <AlternatingRowStyle BackColor="White" BorderColor="#CC66FF" BorderStyle="Solid" />
                            <Columns>
                                <asp:CommandField  ShowDeleteButton="True" ShowEditButton="True" />
                                <asp:TemplateField HeaderText="Categorie" SortExpression="Categorie">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Nom" DataValueField="Id" SelectedValue='<%# Bind("Categorie", "{0}") %>'>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies %>" SelectCommand="SELECT [Id], [Nom] FROM [CateFilm]"></asp:SqlDataSource>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Nom") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                             
                                <asp:TemplateField HeaderText="Titre" SortExpression="Titre">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Titre") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Site", "http://{0}") %>' Text='<%# Eval("Titre") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Directeur" HeaderText="Directeur" SortExpression="Directeur" />
                                <asp:BoundField DataField="Date_publication" HeaderText="Date publication" SortExpression="Date_publication" />
                                <asp:ImageField DataImageUrlField="Image" DataImageUrlFormatString="~/images/{0}" HeaderText="Image">
                             
                                    <ControlStyle BorderStyle="solid" BorderWidth="1px" Height="100px" Width="200px" />
                                </asp:ImageField>
                            </Columns>
                            <FooterStyle BackColor="#f4511e" />
                            <HeaderStyle BorderStyle="none" Font-Bold="True" ForeColor="black" />
                            <PagerStyle BackColor="#f2f2f2" ForeColor="#330099" HorizontalAlign="Center" />
                            <RowStyle BackColor="#f2f2f2" ForeColor="#330099" />
                            <SelectedRowStyle BackColor="#FFCC66" ForeColor="#663399" Font-Bold="True" />
                            <SortedAscendingCellStyle BackColor="#FEFCEB" />
                            <SortedAscendingHeaderStyle BackColor="#AF0101" />
                            <SortedDescendingCellStyle BackColor="#F6F0C0" />
                            <SortedDescendingHeaderStyle BackColor="#7E0000" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:connmovies %>" DeleteCommand="DELETE FROM [films] WHERE [Id] = @Id" InsertCommand="INSERT INTO [films] ([Categorie], [Titre], [Directeur], [Date_publication], [Site], [Image]) VALUES (@Categorie, @Titre, @Directeur, @Date_publication, @Site, @Image)" SelectCommand="SELECT films.Id, films.Categorie, films.Titre, films.Directeur, films.Date_publication, films.Site, films.Image, CateFilm.Nom FROM films INNER JOIN CateFilm ON films.Categorie = CateFilm.Id" UpdateCommand="UPDATE [films] SET [Categorie] = @Categorie, [Titre] = @Titre, [Directeur] = @Directeur, [Date_publication] = @Date_publication, [Site] = @Site, [Image] = @Image WHERE [Id] = @Id">
                            <DeleteParameters>
                                <asp:Parameter Name="Id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Categorie" Type="Int32" />
                                <asp:Parameter Name="Titre" Type="String" />
                                <asp:Parameter Name="Directeur" Type="String" />
                                <asp:Parameter DbType="Date" Name="Date_publication" />
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="Image" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Categorie" Type="Int32" />
                               
                                <asp:Parameter Name="Titre" Type="String" />
                                <asp:Parameter Name="Directeur" Type="String" />
                                <asp:Parameter DbType="Date" Name="Date_publication" />
                                <asp:Parameter Name="Site" Type="String" />
                                <asp:Parameter Name="Image" Type="String" />
                                <asp:Parameter Name="Id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <div align="center">
                        <asp:Label ID="Label2" runat="server" Font-Size="X-Large" ForeColor="#00b503"></asp:Label>
                        <asp:Image ID="Image1" runat="server" ImageUrl="images/ok.png" Height="114px" Visible="False" Width="129px" />
                            </div>
                        <br />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


        </div>
    </div>
</asp:Content>

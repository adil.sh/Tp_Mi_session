﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="projet_Adil.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="style/StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- carousel begin -->
    <br />
    <br />
    <div class ="cara">
  <div class="container">
     <div class="row">
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/thor.png" alt="Thor" style="width:100%;">
        <div class="carousel-caption">
          <h1><b>THOR</b></h1>
          <p>Presentement a l'affiche</p>
        </div>
      </div>

      <div class="item">
        <img src="images/king.jpg" alt="Kingsman" style="width:100%;">
        <div class="carousel-caption">

          <h1><b>KINGSMAN</b></h1>
          <p>Presentement a l'affiche</p>
        </div>
      </div>
    
      <div class="item">
        <img src="images/ninja1.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h1><b>NINJA LEGO</b></h1>
          <p>Presentement a l'affiche</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
         </div>
</div>
   </div>
    <!-- Carousel finish -->
    
    <!-- Jumbotron start --
    <div class="container-fluid">
        <div class="row">
  <div class="jumbotron "  align ="center">
    <h1><img src="images/movies1.png" height="90px" width="90px" />Nos Cinema a Visiter<img src="images/movies1.png" height="90px" width="90px"/></h1> 
   </div>
            </div>
</div>
    <!-- Jumotron finish -->
    <br />
    <br />
    <!-- nos services -->
<div class="services2">
    <div class="container-fluid text-center">
        <br />
  <h2>SERVICES</h2>
  <h4>Choisissez Votre Service</h4>
  <div class="row">
      <br />
    <div class="col-sm-4">
        <br />
        <a href="Webform2.aspx">
        <div class="services1">
            <br />

      <span class="glyphicon glyphicon-eye-open logo-small"></span>
      <h4>AFFICHER</h4>
      <p>Afficher les films dans cinema</p>
            <br />
            </div>
            </a>
    </div>
     
    <div class="col-sm-4 ">
        <br />
         <a href="Webform3.aspx">
        <div class="services1">
            <br />
      <span class="glyphicon glyphicon-cog logo-small"></span>
      <h4>MODIFIEZ</h4>
      <p>Modifiez ou Inserer des films </p>
            <br />
        </div>
             </a>
    </div>
      

    <div class="col-sm-4 ">
        <br />
         <a href="Webform5.aspx">
        <div class="services1">
            <br />
      <span class="glyphicon glyphicon-tint logo-small"></span>
      <h4>METEO</h4>
      <p>Regarder la meteo avant de sortir</p>
            <br />
    </div>
             </a>
        </div>
    </div>
        <br />
    <!-- nos services fin -->
    <!-- Cients say -->
    <div align="center">
    <h2>Quelques mots de nos clients satisfait</h2>
        </div>
<div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
    <h4>"This company is the best. I am so happy with the result!"<br><span style="font-style:normal;">Michael Roe, Vice President, Comment Box</span></h4>
    </div>
    <div class="item">
      <h4>"One word... WOW!!"<br><span style="font-style:normal;">John Doe, Salesman, Rep Inc</span></h4>
    </div>
    <div class="item">
      <h4>"Could I... BE any more happy with this company?"<br><span style="font-style:normal;">Chandler Bing, Actor, FriendsAlot</span></h4>
    </div>

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
    </div>
    <!-- clients end -->
    <!-- card attempt 2 -->

    <div class="cinema1">
        <br />
        <div class="container-fluid text-center bg-grey">
  <h2><strong>Cinema</strong> </h2>
  <h4>Nos cinema a visiter</h4>
            <br />
  <div class="row text-center">
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="images/cinema.jpg" alt="Guzzo"/>
          <br />
        <h3><strong>Cinema Guzzo Montreal</strong></h3>
           <div class="meta">
                            <a href="#">Site Web du Cinema</a>
                        </div>
                        <div class="card-text">
                           Cinema Guzzo vous offre les nouveaute a prix reduite, rabais additionelle pour les etudiants!
                        </div>
                    <br />
                    <div class="card-footer">
                        <span class="float-right">Adresse:</span>
                        <span><i class=""></i>145 Ave Cote Vertu, Montreal, H5W1R6</span>
                    </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="images/cinemar.jpg" alt="Cinema Royal">
          <br />
        <h3><strong>Cinema Royale Montreal</strong></h3>
           <div class="meta">
                            <a href="#">Sit Web du Cinema Royale </a>
                        </div>
        <div class="card-text">
                            Pour l'expreience du Luxe, une soiree de plaisir, Visitez Cinema Royale. Toutes les films sont en IMAX. 
                        </div>
          <br />
                    <div class="card-footer">
                        <span class="float-right">Adresse:</span>
                        <span><i class=""></i>56 Ave Saint Cathrine, Montreal </span>
                    </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="images/cinema2.jpg" alt="Bank scotia">
          <br />
        <h3><strong>Cinema Bank Scotia</strong></h3>
        <div class="meta">
                            <a href="#">Site Web du Cinema Bank Scotia</a>
                        </div>
                        <div class="card-text">
                            Pour une experiance avec toute la famille, jeux pour les enfants, visitez Cinema Bank Scotia
                        </div>
          <br />
                    
                    <div class="card-footer">
                        <span class="float-right">Adresse:</span>
                        <span><i class=""></i>75 Ave WestMount, Montreal</span>
                    </div>
      </div>
    </div>
</div>
            </div>
        <br />
        <br />
        </div>
   
    
    <!-- card 2nd attempt end -->
    
        
</asp:Content>

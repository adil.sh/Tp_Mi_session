﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace projet_Adil
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Label1.Text = string.Empty;
            DetailsView1.ChangeMode(DetailsViewMode.Insert);
            FileUpload image = (FileUpload)DetailsView1.FindControl("fileup_image");

            if (IsPostBack && image.HasFile)
            {
                image.SaveAs(Server.MapPath("~/images/") + image.FileName);
            }
        }

        protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {


            if (e.AffectedRows > 0)
            {
                Label1.Text = "Insertion reussie !! ";
                Image1.Visible = true;

            }
            else
                Label5.Text = "Echec d'insertion";

            Image2.Visible = true;

        }

        protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {

            if (e.AffectedRows > 0)
            {
                Label1.Text = "Mise a jour reussie!";
                Image1.Visible = true;
            }
            else
                Label5.Text = "Echec de mise a jour";
            Image2.Visible = true;
        }

        protected void DetailsView1_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            if (e.AffectedRows > 0)
            {

                Label1.Text = "Operation effectuee avec succes";
                Image1.Visible = true;
            }
            else
                Label5.Text = "Echec!!!";
            Image2.Visible = true;
        }



        protected void DetailsView1_ModeChanged(object sender, EventArgs e)
        {
            if (DetailsView1.CurrentMode != DetailsViewMode.Insert)
            {
                DetailsView1.ChangeMode(DetailsViewMode.Insert);
            }
        }


        protected void check_data(object sender, DetailsViewInsertEventArgs e)
        {
            FileUpload image = (FileUpload)DetailsView1.FindControl("fileup_image");
            if (image.FileContent.Length > 2000000)
            {

            }
            if (((TextBox)DetailsView1.FindControl("txt_t")).Text == "")
            {
                e.Cancel = true;
                Label5.Text = "Titre Invalid ";
                Image2.Visible = true;

            }
            else if (((TextBox)DetailsView1.FindControl("txt_date")).Text != "")
            {
                try
                {
                    DateTime.Parse(((TextBox)DetailsView1.FindControl("txt_date")).Text);
                }
                catch (Exception)
                {
                    e.Cancel = true;
                    Label5.Text = "Format de date   (2012-10-28) ";
                }
            }

        }
    }
}